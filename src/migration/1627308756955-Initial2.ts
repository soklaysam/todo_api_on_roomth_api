import {MigrationInterface, QueryRunner} from "typeorm";

export class Initial21627308756955 implements MigrationInterface {
    name = 'Initial21627308756955'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "product" DROP COLUMN "description"`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "product" ADD "description" text`);
    }

}
