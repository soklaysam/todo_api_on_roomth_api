import {MigrationInterface, QueryRunner} from "typeorm";

export class Initial41627383194937 implements MigrationInterface {
    name = 'Initial41627383194937'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user" ADD "name1" character varying(50)`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "name1"`);
    }

}
