import {MigrationInterface, QueryRunner} from "typeorm";

export class RemoveClientIdFromRefreshToken1627462453387 implements MigrationInterface {
    name = 'RemoveClientIdFromRefreshToken1627462453387'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "refresh_token" DROP COLUMN "clientId"`);
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "name1"`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user" ADD "name1" character varying(50)`);
        await queryRunner.query(`ALTER TABLE "refresh_token" ADD "clientId" character varying NOT NULL`);
    }

}
