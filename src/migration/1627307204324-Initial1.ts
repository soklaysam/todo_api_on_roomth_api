import {MigrationInterface, QueryRunner} from "typeorm";

export class Initial11627307204324 implements MigrationInterface {
    name = 'Initial11627307204324'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "product" DROP COLUMN "remark"`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "product" ADD "remark" text`);
    }

}
