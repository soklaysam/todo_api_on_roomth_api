import {MigrationInterface, QueryRunner} from "typeorm";

export class Initial21627368149222 implements MigrationInterface {
    name = 'Initial21627368149222'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "refresh_token" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "isActive" boolean NOT NULL DEFAULT true, "isArchived" boolean NOT NULL DEFAULT false, "createDateTime" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "createdBy" character varying(300) NOT NULL DEFAULT 'System', "lastChangedDateTime" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "lastChangedBy" character varying(300) NOT NULL DEFAULT 'System', "internalComment" character varying(300), "value" character varying NOT NULL, "userId" character varying NOT NULL, "expiresAt" TIMESTAMP NOT NULL, "clientId" character varying NOT NULL, "ipAddress" character varying NOT NULL, CONSTRAINT "UQ_7f2bc25df3afe0d69f71bd61705" UNIQUE ("value"), CONSTRAINT "PK_b575dd3c21fb0831013c909e7fe" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "product" ADD "description" text NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "product" DROP COLUMN "description"`);
        await queryRunner.query(`DROP TABLE "refresh_token"`);
    }

}
