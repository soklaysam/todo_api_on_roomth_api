import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ormConnection } from './core/config/typeorm.connection';
import { AuthModule } from './feature/auth/auth.module';
import { ProductModule } from './feature/product/product.module';
import { TodoModule } from './feature/todo/todo.module';
import EnvironmentConguration from './core/config/configuration.environment';

@Module({
  imports: [
    ProductModule, 
    AuthModule,
    ConfigModule.forRoot({
      isGlobal: true,
      load: [
        EnvironmentConguration,
      ]
    }),
    TypeOrmModule.forRoot(ormConnection),
    TodoModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {  

  static serverPort: number;
  static isDevelopment: boolean;

  constructor(private configService: ConfigService) {
    AppModule.serverPort = this.configService.get('serverPort');
    AppModule.isDevelopment = this.configService.get('appMode');
  }
}
