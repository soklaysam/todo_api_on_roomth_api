import { BaseEntity } from "../.././../core/entities/base.entity";
import { Column, Entity} from "typeorm";

@Entity()
export class Todo extends BaseEntity {
  @Column({ type: 'varchar', length: 300 })
  name: string;

  @Column({ type: 'text', nullable: true })
  description: string;

  @Column({ default: false })
  isDone: boolean;
}
