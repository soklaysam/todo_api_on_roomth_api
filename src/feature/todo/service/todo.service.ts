import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { BaseService } from 'src/core/service/base.service';
import { Repository } from 'typeorm';
import { Todo } from '../entities/todo.entity';

@Injectable()
export class TodoService extends BaseService<Todo> {
  constructor(@InjectRepository(Todo) private todoRepository: Repository<Todo>){
    super(todoRepository);
  }
}
