import { Controller, Get, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { BaseController } from 'src/core/controller/base.controller';
import { Todo } from '../entities/todo.entity';
import { TodoService } from '../service/todo.service';

@UseGuards(AuthGuard('jwt'))
@Controller('todo')
export class TodoController extends BaseController<Todo>{
  constructor(private readonly todoService: TodoService){
    super(todoService);
  }

  @Get('test/test')
  test(){
    return 'hello';
  }

}
