import { BaseEntity } from "../../../core/entities/base.entity";
import { BeforeInsert, Column, Entity } from "typeorm";
import { genSalt, hash } from "bcryptjs";

@Entity()
export class User extends BaseEntity {

  @Column({ type: 'varchar', length: 50, unique: true })
  login: string;

  @Column({ select: false, type: 'varchar', length: 200 })
  password: string;

  @BeforeInsert()
  async hashPassword(): Promise<void> {
    const salt = await genSalt(12);
    const hashedPassword = await hash(this.password, salt);
    this.password = hashedPassword;
  }

  @Column({ type: 'varchar', length: 50})
  name: string;
}
