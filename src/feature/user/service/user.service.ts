import { Logger, Injectable, UnauthorizedException, BadRequestException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { compare, genSalt, hash } from 'bcryptjs';
import { plainToClass } from 'class-transformer';
import { BaseService } from 'src/core/service/base.service';
import { INVALID_USER_PASSWORD, USER_NAME_ALREADY_USED } from 'src/core/util/message';
import { LoginUserDto } from 'src/feature/auth/dto/login-user.dto';
import { Repository } from 'typeorm';
import { CreateUserDto } from '../dto/create-user.dto';
import { User } from '../entities/user.entity';

@Injectable()
export class UserService extends BaseService<User> {
  private readonly logger = new Logger('UserSerivce');

  constructor(
		@InjectRepository(User)
		private readonly userRepository: Repository<User>,
  ) {
    super(userRepository);
  }

  async login(loginObject: LoginUserDto): Promise<User> {
    const { login, password } = loginObject;

    const user = await this.userRepository
    .createQueryBuilder('user')
    .where("user.login = :login", { login })
    .select("user.id")
    .addSelect("user.password")
    .getOne();

    if (!user) {
      throw new UnauthorizedException(INVALID_USER_PASSWORD);
    }

    const passwordMatch = await compare(password, user.password);

    if (!passwordMatch) {
      throw new UnauthorizedException(INVALID_USER_PASSWORD);
    }

    return user;
  }

  async register(user: CreateUserDto): Promise<User> {
    let isExistingUser = await this.userRepository.findOne({login: user.login});
    if (isExistingUser) {
      throw new BadRequestException(USER_NAME_ALREADY_USED)
    }
    const insertedId = await this.create(plainToClass(User, user));
    return this.get(insertedId)
  }
}