import { Controller, Post, Body } from '@nestjs/common';
import { ApiResponse } from '@nestjs/swagger';
import { BaseController } from 'src/core/controller/base.controller';
import { CreateUserDto } from '../dto/create-user.dto';
import { User } from '../entities/user.entity';
import { UserService } from '../service/user.service';

@Controller('user')
export class UserController extends BaseController<User> {

  constructor(
    private readonly userService: UserService,
  ) {
    super(userService)
  }

  @Post('register')
	@ApiResponse({ status: 201, description: 'The record has been successfully created.'})
	@ApiResponse({ status: 403, description: 'Forbidden.'})
	@ApiResponse({ status: 400, description: 'Bad Request.'})
  async register(@Body() user: CreateUserDto): Promise<User> {
    return this.userService.register(user);
  }
}
