import { IsString, Length } from "class-validator";

export class CreateUserDto {
  @Length(4, 20)
  @IsString()
  login: string;

  @Length(4, 6)
  @IsString()
  password: string;
  
  @Length(4, 20)
  @IsString()
  name: string;
}
