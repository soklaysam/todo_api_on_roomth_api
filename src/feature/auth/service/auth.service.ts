import { Injectable, Logger } from '@nestjs/common';
import { UserService } from 'src/feature/user/service/user.service';
import { LoginUserDto } from '../dto/login-user.dto';
import { LoginResponseHolder } from '../entities/login-response.holder';
import { JwtPayload } from '../jwt-payload';
import { TokenService } from './token.service';

@Injectable()
export class AuthService {
  private readonly logger = new Logger('AuthService');

  constructor(
    private readonly tokenService: TokenService,
    private readonly userService: UserService,
  ) {}

  async login(
    credentials: LoginUserDto,
    ipAddress: string,
  ): Promise<LoginResponseHolder> {
    const loginResults = await this.userService.login(credentials);

    if (!loginResults) {
      return null;
    }

    const payload: JwtPayload = {
      sub: loginResults.id,
    };

    const loginResponse: LoginResponseHolder = await this.tokenService.createAccessToken(
      payload,
    );

    // We save the user's refresh token
    const tokenContent = {
      userId: loginResults.id,
      ipAddress,
    };
    const refresh = await this.tokenService.createRefreshToken(tokenContent);

    loginResponse.refreshToken = refresh;

    return loginResponse;
  }

  async logout(userId: string, refreshToken: string): Promise<any> {
    await this.tokenService.deleteRefreshToken(userId, refreshToken);
  }

  /**
   * Logout the user from all the devices by invalidating all his refresh tokens
   * @param userId The user id to logout
   */
  async logoutFromAll(userId: string): Promise<any> {
    await this.tokenService.deleteRefreshTokenForUser(userId);
  }
}