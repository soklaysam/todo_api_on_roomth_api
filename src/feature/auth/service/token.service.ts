import { Injectable, Logger, UnauthorizedException } from '@nestjs/common';
import { randomBytes } from 'crypto';
import { sign, SignOptions, verify } from 'jsonwebtoken';
import * as moment from 'moment';
import { JwtPayload } from '../jwt-payload';
import { RefreshToken } from '../entities/refresh-token.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { BaseService } from 'src/core/service/base.service';
import { ConfigService } from '@nestjs/config';
import { LoginResponseHolder } from '../entities/login-response.holder';
import { EXPIRED_REFRESH_TOKEN, INVALID_REFRESH_TOKEN } from 'src/core/util/message';
import { v4 as uuidv4 } from 'uuid';

@Injectable()
export class TokenService extends BaseService<RefreshToken>{
  private readonly logger = new Logger(TokenService.name);

  private readonly jwtOptions: SignOptions;
  private readonly jwtKey: string;
  private refreshTokenTtl: number;
  private expiresInDefault: string | number;

  // @todo: should be put in redis cache
  private readonly usersExpired: number[] = [];

  constructor(
		@InjectRepository(RefreshToken)
		private readonly refreshTokenRepository: Repository<RefreshToken>,
    private readonly configService: ConfigService,
    // private readonly mapperService: MapperService,
  ) {
    super(refreshTokenRepository);
    this.expiresInDefault = this.configService.get('jwtExpiresIn');
    this.jwtOptions = { expiresIn: this.expiresInDefault };
    this.jwtKey = this.configService.get('jwtKey');
    this.refreshTokenTtl = this.configService.get('jwtTokenExpiresIn');
  }

  async getAccessTokenFromRefreshToken(
    refreshToken: string,
    oldAccessToken: string,
    ipAddress: string,
  ): Promise<LoginResponseHolder> {
    try {
      // check if refresh token exist in database
      const token = await this.refreshTokenRepository.findOne({ value: refreshToken });
      const currentDate = new Date();
      if (!token) {
        throw new UnauthorizedException(INVALID_REFRESH_TOKEN);
      }
      if (token.expiresAt < currentDate) {
        throw new UnauthorizedException(EXPIRED_REFRESH_TOKEN);
      }
      // Refresh token is still valid
      // Generate new access token
      const oldPayload = await this.validateToken(oldAccessToken, true);
      const payload = {
        sub: oldPayload.sub,
      };
      const accessToken = await this.createAccessToken(payload);
      // Remove old refresh token and generate a new one
      await this.delete(token.id);

      accessToken.refreshToken = await this.createRefreshToken({
        userId: oldPayload.sub,
        ipAddress,
      });

      return accessToken;
    } catch (error) {
      this.logger.error(error);
      throw error;
    }
  }

  async createAccessToken(
    payload: JwtPayload,
    expires = this.expiresInDefault,
  ): Promise<LoginResponseHolder> {
    // If expires is negative it means that token should not expire
    const options = this.jwtOptions;
    expires > 0 ? (options.expiresIn = expires) : delete options.expiresIn;
    // Generate unique id for this token
    options.jwtid = uuidv4();
    const signedPayload = sign(payload, this.jwtKey, options);
    const token: LoginResponseHolder = {
      accessToken: signedPayload,
      expiresIn: expires,
    };

    return token;
  }

  async createRefreshToken(tokenContent: {
    userId: string;
    ipAddress: string;
  }): Promise<string> {
    const { userId, ipAddress } = tokenContent;

    const token = new RefreshToken();

    const refreshToken = randomBytes(64).toString('hex');

    token.userId = userId;
    token.value = refreshToken;
    token.ipAddress = ipAddress;
    token.expiresAt = moment()
      .add(this.refreshTokenTtl, 'd')
      .toDate();

    await this.create(token);

    return refreshToken;
  }

  /**
   * Remove all the refresh tokens associated to a user
   * @param userId id of the user
   */
  async deleteRefreshTokenForUser(userId: string) {
    await this.delete(userId);
    await this.revokeTokenForUser(userId);
  }

  /**
   * Removes a refresh token, and invalidated all access tokens for the user
   * @param userId id of the user
   * @param value the value of the token to remove
   */
  async deleteRefreshToken(userId: string, value: string) {
    await this.delete(userId);
    await this.revokeTokenForUser(userId);
  }

  async decodeAndValidateJWT(token: string): Promise<any> {
    if (token) {
      try {
        const payload = await this.validateToken(token);
        return await this.validatePayload(payload);
      } catch (error) {
        return null;
      }
    }
  }

  async validatePayload(payload: JwtPayload): Promise<any> {
    const tokenBlacklisted = await this.isBlackListed(payload.sub, payload.exp);
    if (!tokenBlacklisted) {
      return {
        id: payload.sub,
      };
    }
    return null;
  }

  private async validateToken(
    token: string,
    ignoreExpiration: boolean = false,
  ): Promise<JwtPayload> {
    return verify(token, this.configService.get('jwtKey'), {
      ignoreExpiration,
    }) as JwtPayload;
  }

  private async isBlackListed(id: string, expire: number): Promise<boolean> {
    return this.usersExpired[id] && expire < this.usersExpired[id];
  }

  private async revokeTokenForUser(userId: string): Promise<any> {
    this.usersExpired[userId] = moment()
      .add(this.expiresInDefault, 's')
      .unix();
  }
}