import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from '../user/entities/user.entity';
import { UserService } from '../user/service/user.service';
import { AuthController } from './controller/auth.controller';
import { RefreshToken } from './entities/refresh-token.entity';
import { AuthService } from './service/auth.service';
import { TokenService } from './service/token.service';
import { JwtStrategy } from './strategies/jwt-strategy';
import { UserModule } from '../user/user.module';

@Module({
  imports: [
  UserModule,
  TypeOrmModule.forFeature([User, RefreshToken]),
  ],
  controllers: [AuthController],
  providers: [AuthService, JwtStrategy, TokenService, UserService],
  exports: [AuthService, TokenService],
})
export class AuthModule {}
