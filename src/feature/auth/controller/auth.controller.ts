import { Controller, Post, Body, Ip, Get, HttpStatus, BadRequestException, UnauthorizedException, Req, Query } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiResponse } from '@nestjs/swagger';
import { LoginUserDto } from '../dto/login-user.dto';
import { LoginResponseHolder } from '../entities/login-response.holder';
import { AuthService } from '../service/auth.service';
import { TokenService } from '../service/token.service';
import { ExtractJwt } from 'passport-jwt';
import { UserService } from 'src/feature/user/service/user.service';
import { CreateUserDto } from 'src/feature/user/dto/create-user.dto';

@Controller('auth')
export class AuthController {

  constructor(
    private  readonly  authService: AuthService,
    private readonly tokenService: TokenService,
    private readonly userService: UserService,
  ) {}
  
  @Post('login')
	@ApiResponse({ status: 200, description: 'Login successfully.'})
	@ApiResponse({ status: 403, description: 'Forbidden.'})
	@ApiResponse({ status: 400, description: 'Bad Request.'})
  async login(@Ip() userIp, @Body() credentials: LoginUserDto): Promise<LoginResponseHolder> {
    return this.authService.login(credentials, userIp);
  } 

  @Get('access_token')
  // Needed to test with swagger page
  @ApiBearerAuth()
  @ApiResponse({ status: HttpStatus.OK, type: LoginUserDto })
  @ApiResponse({ status: HttpStatus.BAD_REQUEST, type: BadRequestException })
  @ApiResponse({ status: HttpStatus.UNAUTHORIZED, type: UnauthorizedException })
  @ApiOperation({ summary: 'AccessToken', description: 'Get a refresh token' })
  async token(
    @Req() req,
    @Ip() userIp,
    @Query('refresh_token') refreshToken?: string,
  ): Promise<LoginResponseHolder> {
    // For now this endpoint only issues new tokens with refresh tokens
    let res: LoginResponseHolder;
    // Get old access token
    const oldAccessToken = ExtractJwt.fromAuthHeaderAsBearerToken()(req);
    res = await this.tokenService.getAccessTokenFromRefreshToken(
      refreshToken,
      oldAccessToken,
      userIp,
    );
    return res;

  }
}
