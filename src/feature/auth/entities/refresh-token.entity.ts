import { BaseEntity } from "../../../core/entities/base.entity";
import { Column, Entity } from "typeorm";
import { IsOptional } from "class-validator";

@Entity()
export class RefreshToken extends BaseEntity {

  @Column({ unique: true })
  value: string;

  @Column()
  userId: string;

  @Column()
  expiresAt: Date;

  @IsOptional()
  @Column()
  ipAddress: string;
}
