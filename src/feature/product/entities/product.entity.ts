import { BaseEntity } from "../.././../core/entities/base.entity";
import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Product extends BaseEntity {

  @Column({ type: 'varchar', length: 300 })
  name: string;

  @Column({ type: 'varchar', length: 50 })
  barcode: string;

  @Column({ type: 'varchar', length: 50 })
  assetCode: string;

  @Column({ type: 'text' })
  description: string;
}
