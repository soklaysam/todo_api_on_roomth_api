import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Product } from '../entities/product.entity';
import { BaseService } from 'src/core/service/base.service';


@Injectable()
export class ProductService extends BaseService<Product> {
  constructor(
		@InjectRepository(Product)
		private readonly productRepository: Repository<Product>
  ) {
		super(productRepository);
	}

  // constructor(
  //   @InjectRepository(Product) private readonly repository: Repository<Product>, 
  //   // private readonly productGeneric: GenericRepository<Product, Product>
  // ){}
  
  // create(createProductDto: CreateProductDto) {
  //   // this.repository.save()
  //   return 'This action adds a new product';
  // }

  // async findAll(query: QueryProductDto) {
  //   // let product: Product[] = await this.repository.find();
  //   // console.log(plainToClass(People, product, { excludeExtraneousValues: true }));
  //   // console.log(product[0])
  //   // console.log(plainToClass(User, product[0], { excludeExtraneousValues: true }));
  //   // return this.productGeneric.findAll(new QueryParamaterDto());
  //   // return this.repository.find();
  // }

  // findOne(id: number) {
  //   return `This action returns a #${id} product`;
  // }

  // // update(id: number, updateProductDto: UpdateProductDto) {
  // //   return `This action updates a #${id} product`;
  // // }

  // remove(id: number) {
  //   return `This action removes a #${id} product`;
  // }
}
