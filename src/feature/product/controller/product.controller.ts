import { Controller, Get, Post, Body, Patch, Param, Delete, Query, ForbiddenException, UseFilters, ParseIntPipe, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { BaseController } from '../.././../core/controller/base.controller';
import { CreateProductDto } from '../dto/create-product.dto';
import { QueryProductDto } from '../dto/query-product.dto';
import { UpdateProductDto } from '../dto/update-product.dto';
import { Product } from '../entities/product.entity';
import { ProductService } from '../service/product.service';

@Controller('product')
@UseGuards(AuthGuard('jwt'))
export class ProductController extends BaseController<Product> {
  // constructor(private readonly productService: ProductService) {}

  constructor(private readonly productService: ProductService) {
		super(productService)
	}

  // @Post()
  // create(@Body() createProductDto: CreateProductDto) {
  //   return this.productService.create(createProductDto);
  // }

  // @Get()
  // findAll(@Query() query: QueryProductDto) {
  //   return this.productService.getAll();
  // }

  // @Get(':id')
  // async findOne(@Param('id', ParseIntPipe) id: Number) {
  //   // throw new ForbiddenException('test');
  //   return this.productService.findOne(+id);
  // }

  // @Patch(':id')
  // update(@Param('id') id: string, @Body() updateProductDto: UpdateProductDto) {
  //   return this.productService.update(+id, updateProductDto);
  // }

  // @Delete(':id')
  // remove(@Param('id') id: string) {
  //   return this.productService.remove(+id);
  // }
}
