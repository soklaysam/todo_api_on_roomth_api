import { Logger, ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import * as helmet from 'helmet';
import { AppModule } from './app.module';
import { HttpExceptionFilter } from './core/filter/http-exception.filter';
import { SnakeCaseInterceptor } from './core/interceptors/snake-case.interceptor';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);  
  const logger = new Logger('Main Application', {timestamp: true});
  const globalPrefix = '/api';

  app.enableCors();
  app.use(helmet());
  
  // Build the swagger doc only in dev mode
  if (AppModule.isDevelopment) {
    const swaggerOptions = new DocumentBuilder()
      .setTitle('Roomth API')
      .setDescription('API documentation for Roomth')
      // .setVersion(version)
      .addBearerAuth()
      .build();

    const swaggerDoc = SwaggerModule.createDocument(app, swaggerOptions);

    SwaggerModule.setup(`${globalPrefix}/swagger`, app, swaggerDoc);
  }

  app.setGlobalPrefix(globalPrefix);

  // Validate query params and body
  app.useGlobalPipes(new ValidationPipe({ transform: true }));

  // Convert exceptions to JSON readable format
  app.useGlobalFilters(new HttpExceptionFilter());

  // Convert all JSON object keys to snake_case
  app.useGlobalInterceptors(new SnakeCaseInterceptor());

  await app.listen(process.env.SERVER_PORT || 3000);
  
  // Log current url of app
  let baseUrl = app.getHttpServer().address().address;
  if (baseUrl === '0.0.0.0' || baseUrl === '::') {
    baseUrl = 'localhost';
  }
  logger.log(`Listening to http://${baseUrl}:${AppModule.serverPort}${globalPrefix}`);
  if (AppModule.isDevelopment) {
    logger.log(
      `Swagger UI: http://${baseUrl}:${AppModule.serverPort}${globalPrefix}/swagger`,
    );
  }
}
bootstrap();
