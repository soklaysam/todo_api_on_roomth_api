import { Get, Post, Delete, Put, Body, Param, HttpStatus} from '@nestjs/common';
import { ApiResponse } from '@nestjs/swagger';
import { BaseEntity } from '../entities/base.entity';
import { IBaseService } from '../service/ibase.service';

export class BaseController<T extends BaseEntity>{

	constructor(private readonly IBaseService: IBaseService<T>) {}

	@Get()
	@ApiResponse({ status: HttpStatus.OK, description: 'Ok'})
	async findAll(): Promise<T[]> {
	  return this.IBaseService.getAll()
	}

	@Get(':id')
	@ApiResponse({ status: HttpStatus.OK, description: 'Entity retrieved successfully.'})
	@ApiResponse({ status: HttpStatus.NOT_FOUND, description: 'Entity does not exist'})
	async findById(@Param('id') id: string): Promise<T> {
	  return this.IBaseService.get(id)
	}

	@Post()
	@ApiResponse({ status: HttpStatus.CREATED, description: 'The record has been successfully created.'})
	@ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.'})
	@ApiResponse({ status: HttpStatus.BAD_REQUEST, description: 'Bad Request.'})
	async create(@Body() entity: T): Promise<string> {
		return this.IBaseService.create(entity);
	}

	@Delete(':id')
	@ApiResponse({ status: HttpStatus.OK, description: 'Entity deleted successfully.'})
	@ApiResponse({ status: HttpStatus.BAD_REQUEST, description: 'Bad Request.'})
	async delete(@Param('id') id: string) {
	  this.IBaseService.delete(id);
	}

	@Put()
	@ApiResponse({ status: HttpStatus.BAD_REQUEST, description: 'Bad Request.'})
	@ApiResponse({ status: HttpStatus.OK, description: 'Entity deleted successfully.'})
	async update(@Body() entity: T): Promise<T> {
	  return this.IBaseService.update(entity);
	}

}