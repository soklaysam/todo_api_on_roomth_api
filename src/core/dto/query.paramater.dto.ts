import { IsNumberString, IsOptional, IsString } from "class-validator";

export class QueryParamaterDto {
  @IsOptional()
  @IsNumberString()
  limit: Number;

  @IsOptional()
  @IsString()
  name: String;
}
