export interface IBaseService<T> {
    
  getAll(): Promise<T[]>;
  get(id: string): Promise<T>;
  update(entity: T): Promise<T>;
  create(entity: T): Promise<string>;
  delete(id: string);
}