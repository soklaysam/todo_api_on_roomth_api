import { BadGatewayException, Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { BaseEntity } from '../entities/base.entity';
import { IBaseService } from './ibase.service';

// TODO: We should implementation Td (DTO) one more, it is good create, update, and delete.
@Injectable()
export class BaseService<Te extends BaseEntity> implements IBaseService<Te>{
	constructor(
    private readonly genericRepository: Repository<Te>
  ) {}

  create(entity: any): Promise<string> {
	  try {
      return new Promise<string> ((resolve, reject) => {
        this.genericRepository.save(entity)
        .then(created=> resolve(created.id))
        .catch(err => reject(err))
      })
    }
		catch(error) {
			throw new BadGatewayException(error);
		}
  }

  getAll(): Promise<Te[]> {
	  try {
		  return <Promise<Te[]>>this.genericRepository.find();
	  } catch (error) {
      throw new BadGatewayException(error);
    }
  }

  async get(id: string): Promise<Te> {
    try {
      await this.genericRepository.findOne(id)
    } catch (error) {
      throw new BadGatewayException(error);
    }
  	return <Promise<Te>>this.genericRepository.findOne(id);
  }

  delete(id: string) {
    try {
      this.genericRepository.delete(id)
    } catch (error) {
      throw new BadGatewayException(error);
    }
  }

  softDelete(id: string) {
    try {
      this.genericRepository.softDelete(id)
    } catch (error) {
      throw new BadGatewayException(error);
    }
  }

  update(entity: BaseEntity): Promise<any>{
    try {
      return new Promise<any> ((resolve, reject) => {
        this.genericRepository.findOne(entity.id).then(responseGet => {
          try {
            if (responseGet == null) {
              reject('Not existing') 
            }
            let retrievedEntity: any = responseGet as any
            this.genericRepository.save(retrievedEntity)
            .then(response => resolve(response))
            .catch(err => reject(err))
          }
          catch(e) {
            reject(e)
          }
        })
        .catch(err => reject(err))
        }) 
    } catch (error) {
      throw new BadGatewayException(error);
    }
  }
}