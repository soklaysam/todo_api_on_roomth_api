export const USER_NAME_ALREADY_USED = 'User Name is already used!';
export const INVALID_USER_PASSWORD = 'Invalid user name and password!';
export const INVALID_REFRESH_TOKEN = 'Invalid refresh token';
export const EXPIRED_REFRESH_TOKEN = 'Expired refresh token';