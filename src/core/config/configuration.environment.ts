
var dotenv = require('dotenv');

let path = `.env.${process.env.NODE_ENV || 'development'}`;

let dotenv_config = dotenv.config({ path: path });

if (dotenv_config.error) {
  throw dotenv_config.error;
}

export default () => ({
  appMode: process.env.APP_MODE,
  serverPort: parseInt(process.env.SERVER_PORT, 10) || 3000,
  database: {
    type: 'postgres',
    host: process.env.POSTGRES_HOST || 'locahost',
    port: parseInt(process.env.POSTGRES_PORT) || 5433,
    username: process.env.POSTGRES_USER || 'postgres',
    password: process.env.POSTGRES_PASSWORD || 'postgres',
    database: process.env.POSTGRES_DATABASE,
  },
  runMigration: process.env.RUN_MIGRATIONS,
  jwtKey: process.env.JWT_KEY,
  jwtExpiresIn: process.env.JWT_EXPIRES_IN,
  jwtTokenExpiresIn: process.env.JWT_REFRESH_EXPIRES_IN,
});