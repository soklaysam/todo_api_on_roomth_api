
// TODO: I will clean up this file later, becuause I need to use database migration versioning.
// Environment variable, we should take it from one place (configuration.environment.ts)
import { TypeOrmModuleOptions } from "@nestjs/typeorm";

var dotenv = require('dotenv');

let path = `.env.${process.env.NODE_ENV || 'development'}`;

let dotenv_config = dotenv.config({ path: path });

if (dotenv_config.error) {
  throw dotenv_config.error;
}

let ormConnection: TypeOrmModuleOptions = {
  type: 'postgres',
  host: process.env.POSTGRES_HOST || 'locahost',
  port: parseInt(process.env.POSTGRES_PORT) || 5433,
  username: process.env.POSTGRES_USER || 'postgres',
  password: process.env.POSTGRES_PASSWORD || 'postgres',
  database: process.env.POSTGRES_DATABASE,
  // We can also use entities: [EntityName], it will work with webpack, but this one ["dist/**/*.entity{.ts,.js}"] is not working with webpage
  entities: ["dist/**/**/*.entity{.js,.ts}"], 
}

let migrationConfig = Object.assign({}, ormConnection, {
  migrationsTableName: 'migration',
  migrations: ['src/migration/*{.js,.ts}'],
  cli: {
    migrationsDir: 'src/migration'
  },
  synchronize: false,
});


export {ormConnection, migrationConfig}